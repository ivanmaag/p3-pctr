
/**
 * Práctica 3 - PCTR 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

public class ProdEscalar {

    private static double [] v1 = new double[10];
    private static double [] v2 = new double[10];

    /**
     * Método main. Rellena dos vectores y calcula el producto escalar de ambos de forma secuencial.
     * @param args No recoge argumentos.
     */
    public static void main(String[] args) {
        
        double productoEscalar = 0;

        //Inicializamos el vector v1 todo a 3
        for (int i=0; i<v1.length; ++i) {
            v1[i] = 1;
            //System.out.print(v1[i] + ", ");
        }
        //Inicializamos también el vector v2 todo a 3
        for (int i=0; i<v2.length; ++i) {
            v2[i] = 1;
            //System.out.print(v2[i] + ", ");
        }
        
        for (int i=0; i<v1.length; ++i) {
            productoEscalar += (v1[i] * v2[i]);
        }
            
        System.out.println("Producto escalar: " + productoEscalar);
    }
}
