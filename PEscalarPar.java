
/**
 * Práctica 3 - PCTR 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

public class ProdEscalar2Hilos {

    private int idHebra = 2;

    private static double [] v1 = new double[100];
    private static double [] v2 = new double[100];

    public PEscalarPar(int idHebra,
                       int ini,
                       int fin) {

        this.idHebra = idHebra;
        this.ini = ini;
        this.fin = fin;
    }
    //Método run
    public void run() {
        double [] productoParcial = new double[idHebra];
        //Multiplico cada valor del vector v1 por el vector v2
        switch (idHebra) {
        case 0: for (int i=0; i<49; ++i) {
                    productoParcial[0] += (v1[i] * v2[i]);
                }
                break;
        case 1: for (int i=50; i<99; ++i) {
                    productoParcial[1] += (v1[i] * v2[i]);
                }
                break;
        }
    }

    /**
     * Método main. Rellena dos vectores y calcula el producto escalar de ambos de forma secuencial.
     * @param args No recoge argumentos.
     */
    public static void main(String[] args) {
        
        double productoEscalar = 0;

        //Inicializamos el vector v1 todo a 3
        for (int i=0; i<v1.length; ++i) {
            v1[i] = 1;
        }
        //Inicializamos también el vector v2 todo a 3
        for (int i=0; i<v2.length; ++i) {
            v2[i] = 1;
        }
        
        productoEscalar = productoParcial[0] + productoParcial[1];
            
        System.out.println("Producto escalar: " + productoEscalar);
    }
}
