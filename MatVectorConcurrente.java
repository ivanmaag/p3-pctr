
/**
 * Práctica 3 - PCTR 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

import java.util.Random;
import java.util.Scanner;

public class MatVectorConcurrente implements Runnable {
    
    //Matriz para realizar la operación
    private static int[][] matrix;
    
    //Vector será el vector que se multiplique por la matriz
    //Result será el vector donde se almacene el resultado
    private static int[] vector, result;
    
    //Inicio y fin de las filas donde operarán cada hilo
    private int inicio, fin;

    /**
     * Constructor de la clase
     * @param inicio representa el inicio del subconjunto de filas
     * @param fin representa el final del subconjunto de filas
     */
    public MatVectorConcurrente(int inicio, int fin) {
        this.inicio = inicio;
        this.fin = fin;
    }

    /**
     * Método observador para consultar el resultado
     */
    public int[] GetResult() {
        return result;
    }

    /**
     * Método para inicializar el vector resultado, el vector y la matriz
     * Además estos dos últimos se rellenarán con números aleatorios
     * @param tam tamaño de cada uno de los elementos
     */
    public void InicializarMatrizVectores(int tam) {
        matrix = new int[tam][tam];
        vector = new int[tam];
        result = new int[tam];
        Random random = new Random();

        for (int i=0; i<matrix.length; i++) {
            for (int j=0; j<matrix[i].length; j++) {
                matrix[i][j] = random.nextInt();
            }
            vector[i] = random.nextInt();
        }
    }

    /**
     * Sobrecarga del método run donde los hilos realizarán
     * los cálculos.
     */
    public void run() {
        for (int i=inicio; i<fin; i++) {
            for (int j=0; j<matrix[i].length; j++) {
                result[i] += matrix[i][j] * vector[i];
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {

        Thread[] arrayHilos; 
        int ventana, tam, tamHilosRunnable, ini = 0;
        boolean flagOnce = false;
        MatVectorConcurrente[] matVectorC;
        Scanner s = new Scanner(System.in);
        
        System.out.print("Introduzca numero de hilos: ");
        tamHilosRunnable = s.nextInt();
        arrayHilos = new Thread[tamHilosRunnable];
        matVectorC = new MatVectorConcurrente[tamHilosRunnable];
        
        System.out.print("Introduzca tamaño del vector y de la matriz: ");
        tam = s.nextInt();

        ventana=tam/tamHilosRunnable;

        long iniCronom = System.currentTimeMillis();
        for (int i=0; i<matVectorC.length-1; i++) {
            matVectorC[i] = new MatVectorConcurrente(ini, ini + ventana);
            if (!flagOnce) {
                matVectorC[i].InicializarMatrizVectores(tam);
                flagOnce = true;
            }
            ini += ventana;
        }

        matVectorC[matVectorC.length-1] = new MatVectorConcurrente(ini, tam);

        for (int i=0; i<arrayHilos.length; i++) {
            arrayHilos[i] = new Thread(matVectorC[i]);
            arrayHilos[i].start();
        }

        for (int i=0; i<arrayHilos.length; i++) {
            arrayHilos[i].join();
        }

        long finCronom = System.currentTimeMillis();
        System.out.println("Ha tardado: " + (finCronom - iniCronom) + " milisegundos");
    }
}
