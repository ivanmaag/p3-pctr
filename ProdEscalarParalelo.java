
/**
 * Práctica 3 - PCTR 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

import java.util.Scanner;

public class ProdEscalarParalelo extends Thread {

    // Variables para saber la id de cada hebra, el inicio del subcojunto del vector
    // y el fin del subconjunto del vector sobre el que operar
    private int idHebra, inicio, fin;

    // Vectores sobre los que realizar las operaciones y vector donde guardar el
    // resultado
    private static double[] productoParcial, v1, v2;

    /**
     * Constructor de la clase
     * 
     * @param idHebra id de la hebra
     * @param inicio  posición inicial del subconjunto en la que iniciar
     * @param fin     posición final del subconjunto
     */
    public ProdEscalarParalelo(int idHebra,
                               int inicio,
                               int fin) {

        this.idHebra = idHebra;
        this.inicio = inicio;
        this.fin = fin;
    }

    /**
     * Método para incializar los vectores.
     * 
     * @param tam     Tamaño de los vectores
     * @param tamProd Número de hilos
     */
    public void InicializarVectores(int tam, int tamProd) {
        v1 = new double[tam];
        v2 = new double[tam];
        productoParcial = new double[tamProd];

        for (int i = 0; i < v1.length; i++) {
            v1[i] = i + 1;
            v2[i] = i + 1;
        }
    }

    /**
     * Método modificador para guardar el cálculo realizado por cada hebra
     * 
     * @param idHebra Hebra que realiza el cálculo
     * @param result  resultado calculado
     */
    public void SetResult(int idHebra, double result) {
        productoParcial[idHebra] = result;
    }

    /**
     * Método observador para obtener el vector donde almacenamos el resultado
     * 
     * @return
     */
    public double[] GetResult() {
        return productoParcial;
    }

    /**
     * Sobrecarga del método run. En él calculamos el producto interno y se lo
     * envíamos al método modificador
     */
    public void run() {
        double result = 0;

        for (int i = inicio; i < fin; i++) {
            result += v1[i] * v2[i];
        }

        SetResult(this.idHebra, result);
    }

    public static void main(String[] args) throws InterruptedException {

        Scanner s = new Scanner(System.in);
        boolean flagOnce = false;
        int nHilos, ventana, tam, ini = 0;
        ProdEscalarParalelo[] prodEscalarParalelo;

        System.out.print("Introduzca numero de hilos: ");
        nHilos = s.nextInt();
        System.out.print("Introduzca tamaño de los vectores: ");
        tam = s.nextInt();
        ventana = tam / nHilos;
        prodEscalarParalelo = new ProdEscalarParalelo[nHilos];
        
        long inicCronom = System.currentTimeMillis();
        int i = 0;
        for (i = 0; i < prodEscalarParalelo.length - 1; i++) {
            prodEscalarParalelo[i] = new ProdEscalarParalelo(i, ini, ini + ventana);
            if (!flagOnce) {
                prodEscalarParalelo[i].InicializarVectores(tam, nHilos);
                flagOnce = true;
            }

            ini += ventana;
            prodEscalarParalelo[i].start();
        }
        prodEscalarParalelo[prodEscalarParalelo.length - 1] = new ProdEscalarParalelo(i, ini, tam);
        prodEscalarParalelo[prodEscalarParalelo.length - 1].start();
        for (i = 0; i < prodEscalarParalelo.length; i++) {
            prodEscalarParalelo[i].join();
        }

        long finCronom = System.currentTimeMillis();
        System.out.println("Ha tardado: " + (finCronom - inicCronom) + " milisegundos");
    }
}
