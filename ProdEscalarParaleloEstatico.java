
/**
 * Práctica 3 - PCTR 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

public class ProdEscalarParaleloEstatico extends Thread {
    // Variables para saber la id de cada hebra, el inicio del subcojunto del vector
    // y el fin del subconjunto del vector sobre el que operar
    private int idHebra, inicio, fin;

    // Vectores sobre los que realizar las operaciones y vector donde guardar el
    // resultado
    private static double[] productoParcial, v1, v2;

    /**
     * Constructor de la clase
     * 
     * @param idHebra id de la hebra
     * @param inicio  posición inicial del subconjunto en la que iniciar
     * @param fin     posición final del subconjunto
     */
    public ProdEscalarParaleloEstatico(int idHebra, int inicio, int fin) {
        this.idHebra = idHebra;
        this.inicio = inicio;
        this.fin = fin;
    }

    /**
     * Método para incializar los vectores.
     * 
     * @param tam     Tamaño de los vectores
     * @param tamProd Número de hilos
     */
    public void InicializarVectores(int tam, int tamProd) {
        
        v1 = new double[tam];
        v2 = new double[tam];
        productoParcial = new double[2];

        for (int i=0; i<v1.length; i++) {
            v1[i]=i+1;
            v2[i]=i+1;
        }
    }

    /**
     * Método modificador para guardar el cálculo realizado por cada hebra
     * 
     * @param idHebra Hebra que realiza el cálculo
     * @param result  resultado calculado
     */
    public void SetResult(int idHebra, double result) {
        productoParcial[idHebra] = result;
    }

    /**
     * Método observador para obtener el vector donde almacenamos el resultado
     * 
     * @return
     */
    public double[] GetResult() {
        return productoParcial;
    }

    /**
     * Sobrecarga del método run. En él calculamos el producto interno y se lo
     * envíamos al método modificador
     */
    public void run() {
        double result = 0;

        for (int i=inicio; i<=fin; i++) {
            result += v1[i] * v2[i];
        }

        SetResult(this.idHebra, result);
    }

    public static void main(String[] args) throws InterruptedException {

        ProdEscalarParaleloEstatico h1 = new ProdEscalarParaleloEstatico(0, 0, 49);
        ProdEscalarParaleloEstatico h2 = new ProdEscalarParaleloEstatico(1, 50, 99);

        h1.InicializarVectores(100, 2);

        long inicCronom = System.currentTimeMillis();
        h1.start();
        h2.start();
        h1.join();
        h2.join();
        long finCronom = System.currentTimeMillis();
        System.out.println("Ha tardado: " + (finCronom - inicCronom) + " milisegundos");

        double res = 0;
        for (double r : h1.GetResult()) {
            res += r;
        }

        System.out.println("El resultado es: " + res);
    }
}
