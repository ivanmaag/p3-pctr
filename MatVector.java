
/**
 * Práctica 3 - PCTR 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

import java.util.Random;

public class MatVector {

    private int[][] matrix = new int[10000][10000];
    private int[] vector = new int[10000];
    private int[] result = new int[10000];

    public void SetMatrixAndVector() {
        Random r = new Random();

        for (int i=0; i<matrix.length; i++) {
            for (int j=0; j<matrix[i].length; j++) {
                matrix[i][j] = r.nextInt();
            }
            vector[i] = r.nextInt();
        }
    }

    public void CalculateProduct() {
        for (int i=0; i<matrix.length; i++) {
            for (int j=0; j<matrix[i].length; j++) {
                result[i] += matrix[i][j] * vector[j];
            }
        }
    }

    public static void main(String[] args) {
        MatVector matV = new MatVector();
        matV.SetMatrixAndVector();

        long iniCronom = System.currentTimeMillis();
        matV.CalculateProduct();
        long finCronom = System.currentTimeMillis();

        System.out.println("Tiempo tardado: " + (finCronom - iniCronom));
    }
}
