
/**
 * Práctica 3 - PCTR 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

public class ProdEscalar {

    //Vectores sobre los que se realizan las operaciones
    private double[] v1 = new double[1000000];
    private double[] v2 = new double[1000000];
    
    /**
     * Método para asignarle valores a los elementos de cada vector
     */
    public void InicializarVectores()
    {
        for(int i = 0; i < v1.length; i++)
        {
            v1[i] = i + 1;
            v2[i] = i + 1;
        }
    }

    /**
     * Método donde se realiza el cálculo del producto interno
     * @return cálculo realizado
     */
    public double CalcularProdEscalar()
    {
        double res = 0;
        for(int i = 0; i < v1.length; i ++)
        {
            res += v1[i] * v2[i];
        }
        return res;
    }

    public static void main (String[] args)
    {
        ProdEscalar prodE = new ProdEscalar();
        prodE.InicializarVectores();

        long iniCronom = System.currentTimeMillis();
        System.out.println("El producto escalar es: " + prodE.CalcularProdEscalar());
        long finCronom = System.currentTimeMillis();

        System.out.println("Tiempo tardado: " + (finCronom - iniCronom));
    }
}
