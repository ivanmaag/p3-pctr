
/**
 * Práctica 3 - PCTR 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

public class ProdEscalar2Hilos extends Thread {

    private int idHebra = 2;
    private int ini;
    private int fin;

    private static double[] u = new double[100];
    private static double[] v = new double[100];
    private static double prodEscalar = 0;
    private static double[] prodParcial = new double[2];

    public ProdEscalar2Hilos(int idHebra,
                             int ini,
                             int fin) {

        this.idHebra = idHebra;
        this.ini = ini;
        this.fin = fin;
    }

    // Método run
    public void run() {
        for (int i=ini; i<=fin; ++i) {
            prodEscalar += (u[i] * v[i]);
        }
        prodParcial[idHebra] = prodEscalar;
    }

    /**
     * Método main. Rellena dos vectores y calcula el producto escalar de ambos de
     * forma secuencial.
     * 
     * @param args No recoge argumentos.
     */
    public static void main(String[] args) throws InterruptedException {

        // Inicializamos el vector u todo a 1
        for (int i=0; i<u.length; ++i) {
            u[i]=1;
        }
        // Inicializamos también el vector v todo a 1
        for (int i=0; i<v.length; ++i) {
            v[i]=1;
        }

        ProdEscalar2Hilos t1 = new ProdEscalar2Hilos(0, 0, 49);
        ProdEscalar2Hilos t2 = new ProdEscalar2Hilos(1, 50, 99);
        Thread h1 = new Thread(t1);
        Thread h2 = new Thread(t2);
        h1.start();
        h2.start();
        h1.join();
        h2.join();

        prodEscalar = prodParcial[0] + prodParcial[1];

        System.out.println("Producto escalar: " + prodEscalar);
    }
}
